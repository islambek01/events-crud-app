import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Event } from '../models/event.model';
import { EVENT_TYPES_LIST } from '../const/common';
import { NotificationService } from '../services/notification.service';

@Component({
  selector: 'app-event-form',
  templateUrl: './event-form.component.html',
  styleUrls: ['./event-form.component.scss']
})
export class EventFormComponent implements OnInit {

  @Input() currentEvent: Event = new Event();
  @Input() pageTitle: string = '';
  @Input() buttonEnable: boolean = true;
  @Input() buttonText: string = '';
  @Output() cancelConfirmed = new EventEmitter<Event>();
  @Output() saveConfirmed = new EventEmitter<Event>();

  eventTypes = EVENT_TYPES_LIST;

  constructor(private notificationService: NotificationService) {
  }

  get isBirthdaySelected(): boolean {
    return this.currentEvent.type.id === this.eventTypes[0].id;
  }

  get isPartySelected(): boolean {
    return this.currentEvent.type.id === this.eventTypes[1].id;
  }

  get isOtherSelected(): boolean {
    return this.currentEvent.type.id === this.eventTypes[2].id;
  }

  ngOnInit(): void {
  }

  save(): void {
    if (!this.currentEvent.title) {
      this.notificationService.update('Введите название!', 'error');
      return;
    }
    this.saveConfirmed.emit(this.currentEvent);
  }

  cancel(): void {
    this.cancelConfirmed.emit();
  }

}
