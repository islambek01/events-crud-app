export interface EventType {
  id: string,
  title: string;
}

export class Event {
  id: string = '';
  date: Date = new Date();
  title: string = '';
  budget: number = 0;
  type: EventType = {id: '', title: ''};
  time: string = '';
  address: string = '';
  note: string = '';
}
