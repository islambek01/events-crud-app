import { Component, OnInit } from '@angular/core';
import { BUTTON_TEXTS, EVENT_TYPES_LIST, NOTIFY_MESSAGES, PAGE_TITLES } from '../const/common';
import { Event } from '../models/event.model';
import { EventService } from '../services/event.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NotificationService } from '../services/notification.service';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.scss']
})
export class CreateEventComponent implements OnInit {

  newEvent: Event = new Event();
  buttonEnable = true;
  buttonCreate = BUTTON_TEXTS.create;
  eventTypes = EVENT_TYPES_LIST;
  pageTitle = PAGE_TITLES.create;
  selectedDate = new Date();

  constructor(private eventService: EventService,
              private router: Router,
              private route: ActivatedRoute,
              private notificationService: NotificationService) {
    this.newEvent.type = this.eventTypes[0];

    this.route.queryParams.subscribe((params: Params) => {
      const date = params.date;
      if (date) {
        this.newEvent.date = date;
        this.selectedDate = date;
      }
      else {
        this.newEvent.date = new Date();
      }
    });

  }

  ngOnInit(): void {
  }

  createEvent($event: any): void {
    const newEvent = $event;
    newEvent.id = new Date().getTime();
    this.eventService.create(newEvent);
    this.notificationService.update(NOTIFY_MESSAGES.EVENT_CREATED_SUCCESSFULLY, 'success');
    this.newEvent = new Event();
    this.newEvent.type = this.eventTypes[0];
    this.newEvent.date = this.selectedDate;
  }

  cancelCreateEvent(): void {
    this.router.navigateByUrl('/');
  }

}
