import { Component, OnInit } from '@angular/core';
import { EventService } from '../services/event.service';
import { Event } from '../models/event.model';
import { NOTIFY_MESSAGES } from '../const/common';
import { NotificationService } from '../services/notification.service';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.scss']
})
export class EventListComponent implements OnInit {
  // eventList: Event[] = [];
  selectedEvent: Event = new Event();
  isDeleteEvent = false;
  buttonEnable = true;
  selectedDateValue = '';

  constructor(private eventService: EventService,
              private notificationService: NotificationService) {
    this.selectedDateValue = this.formatDate(null);
  }

  get eventList(): Event[] {
    const eventList = this.eventService.getEvents();
    return eventList.filter((e: Event) => this.formatDate(e.date) == this.selectedDateValue);
  }

  ngOnInit(): void {
  }

  formatDate(value: any): any {
    const today = value ? new Date(value) : new Date();
    let date = new Date(today.getFullYear(), today.getMonth() + 1, today.getDate());
    let month: any = date.getUTCMonth();
    let day: any = date.getDate();
    const year = date.getUTCFullYear();
    if (month < 10) {
      month = '0' + month;
    }
    if (day < 10) {
      day = '0' + day;
    }

    return year + '-' + month + '-' + day;
  }

  confirmDelete(event: Event): void {
    this.selectedEvent = event;
    this.isDeleteEvent = true;
  }

  deleteEvent(): void {
    this.eventService.delete(this.selectedEvent.id);
    this.notificationService.update(NOTIFY_MESSAGES.EVENT_DELETED_SUCCESSFULLY, 'success');
    this.closeModal();
  }

  closeModal(): void {
    this.isDeleteEvent = false;
  }
}
