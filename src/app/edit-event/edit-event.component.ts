import { Component, OnInit } from '@angular/core';
import { Event } from '../models/event.model';
import { BUTTON_TEXTS, EVENT_TYPES_LIST, NOTIFY_MESSAGES, PAGE_TITLES } from '../const/common';
import { EventService } from '../services/event.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from '../services/notification.service';

@Component({
  selector: 'app-edit-event',
  templateUrl: './edit-event.component.html',
  styleUrls: ['./edit-event.component.scss']
})
export class EditEventComponent implements OnInit {

  selectedEvent: Event = new Event();
  eventId: string | null = '';
  buttonEnable = true;
  buttonCreate = BUTTON_TEXTS.save;
  eventTypes = EVENT_TYPES_LIST;
  pageTitle = PAGE_TITLES.edit;

  constructor(private eventService: EventService,
              private route: ActivatedRoute,
              private router: Router,
              private notificationService: NotificationService) {
    this.route.paramMap.subscribe(params => {
      this.eventId = params.get('id');
      this.selectedEvent = this.eventService.getEventById(this.eventId)
    });
  }

  ngOnInit(): void {
  }

  updateEvent($event: any): void {
    const updatedEvent = $event;
    this.eventService.update(updatedEvent);
    this.notificationService.update(NOTIFY_MESSAGES.EVENT_UPDATED_SUCCESSFULLY, 'success');
    this.router.navigateByUrl('/');
  }

  cancelUpdateEvent(): void {
    this.router.navigateByUrl('/');
  }
}
