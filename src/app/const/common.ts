export const EVENT_TYPES_LIST = [
  {
    id: '1',
    title: 'Праздничные дни'
  },
  {
    id: '2',
    title: 'Мероприятия'
  },
  {
    id: '3',
    title: 'Пометки / Другое'
  }
];

export const PAGE_TITLES = {
  create: 'Добавить событие',
  edit: 'Редактировать событие',
};

export const BUTTON_TEXTS = {
  save: 'Сохранить',
  create: 'Добавить',
  cancel: 'Отмена',
};

export const NOTIFY_MESSAGES = {
  EVENT_CREATED_SUCCESSFULLY: 'Событие успешно создано',
  EVENT_UPDATED_SUCCESSFULLY: 'Событие успешно обновлено',
  EVENT_DELETED_SUCCESSFULLY: 'Событие успешно удалено'
};
