import { Injectable } from '@angular/core';
import { Event } from '../models/event.model';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  eventKey = 'eventList';

  constructor() {
  }

  getEvents(): Event[] | [] {
    const events = localStorage.getItem(this.eventKey);
    if (!events) {
      return [];
    }
    return JSON.parse(events);
  }

  getEventById(id: string | null): Event {
    const eventList = this.getEvents();
    const event = eventList.find((e: Event) => e.id == id);
    return event || new Event();
  }

  create(newEvent: Event): any {
    const eventList: Event[] = this.getEvents();
    eventList.push(newEvent);
    localStorage.setItem(this.eventKey, JSON.stringify(eventList));
  }

  update(event: Event): any {
    const eventList = this.getEvents();
    const index = eventList.findIndex((e: Event) => e.id != event.id);
    eventList[index] = event;
    localStorage.setItem(this.eventKey, JSON.stringify(eventList));
  }

  delete(id: string): any {
    const eventList = this.getEvents();
    const newEventList = eventList.filter((e: Event) => e.id != id);
    localStorage.setItem(this.eventKey, JSON.stringify(newEventList));
  }
}
